package app.bopratice.api.customer;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

public class SearchCustomerAJAXResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "customers")
    public List<Customer> customers;

    public static class Customer {
        @NotNull
        @Property(name = "customer_id")
        public Long customerId;

        @NotNull
        @NotBlank
        @Property(name = "email")
        public String email;

        @NotNull
        @NotBlank
        @Property(name = "first_name")
        public String firstName;

        @NotBlank
        @Property(name = "last_name")
        public String lastName;

        @NotNull
        @Property(name = "updated_time")
        public LocalDateTime updatedTime;
    }
}
