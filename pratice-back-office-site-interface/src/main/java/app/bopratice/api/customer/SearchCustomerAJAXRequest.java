package app.bopratice.api.customer;

import core.framework.api.validate.Min;
import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

public class SearchCustomerAJAXRequest {
    @NotNull
    @Min(0)
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @Min(1)
    @QueryParam(name = "limit")
    public Integer limit;

    @QueryParam(name = "email")
    public String email;

    @QueryParam(name = "first_name")
    public String firstName;

    @QueryParam(name = "last_name")
    public String lastName;
}
