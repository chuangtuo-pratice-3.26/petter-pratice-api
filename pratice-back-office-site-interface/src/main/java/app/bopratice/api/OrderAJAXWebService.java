package app.bopratice.api;

import app.bopratice.api.order.DeleteOrderAJAXResponse;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface OrderAJAXWebService {
    @DELETE
    @Path("/ajax/order/:id")
    DeleteOrderAJAXResponse delete(@PathParam("id") Long id);
}
