package app.bopratice.api;

import app.bopratice.api.customer.AJAXCustomerView;
import app.bopratice.api.customer.SearchCustomerAJAXRequest;
import app.bopratice.api.customer.SearchCustomerAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface CustomerAJAXWebService {

    @PUT
    @Path("/ajax/customer/:id/inactive-status")
    AJAXCustomerView inactive(@PathParam("id") Long id);

    @GET
    @Path("/ajax/customer")
    SearchCustomerAJAXResponse search(SearchCustomerAJAXRequest request);
}
