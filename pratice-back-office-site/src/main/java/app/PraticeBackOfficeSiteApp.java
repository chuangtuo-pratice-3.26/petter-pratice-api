package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

public class PraticeBackOfficeSiteApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8089);
        load(new SystemModule("app.properties"));
//        load(new SystemModule("sys.properties"));
        load(new BOWebModule());
    }
}
