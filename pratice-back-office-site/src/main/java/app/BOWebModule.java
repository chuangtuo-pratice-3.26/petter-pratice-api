package app;

import app.bopratice.api.CustomerAJAXWebService;
import app.bopratice.api.OrderAJAXWebService;
import app.customer.api.BOCustomerWebService;
import app.order.api.BOOrderWebService;
import app.web.service.customer.CustomerAJAXWebServiceImpl;
import app.web.service.order.OrderAJAXWebServiceImpl;
import core.framework.module.Module;

public class BOWebModule extends Module {
    @Override
    protected void initialize() {
//        log().appendToConsole();
        dealClient();
        dealService();
    }

    private void dealClient() {
        api().client(BOCustomerWebService.class, requiredProperty("app.customerServiceURL"));
        api().client(BOOrderWebService.class, requiredProperty("app.orderServiceURL"));
    }

    private void dealService() {
        api().service(CustomerAJAXWebService.class, bind(CustomerAJAXWebServiceImpl.class));
        api().service(OrderAJAXWebService.class, bind(OrderAJAXWebServiceImpl.class));
    }
}
