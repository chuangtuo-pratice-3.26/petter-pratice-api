package app.web.service.customer;

import app.bopratice.api.CustomerAJAXWebService;
import app.bopratice.api.customer.AJAXCustomerView;
import app.bopratice.api.customer.SearchCustomerAJAXRequest;
import app.bopratice.api.customer.SearchCustomerAJAXResponse;
import app.customer.api.BOCustomerWebService;
import app.customer.api.customer.BOCustomerView;
import app.customer.api.customer.BOSearchCustomerRequest;
import app.customer.api.customer.BOSearchCustomerResponse;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

import java.util.ArrayList;


public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    BOCustomerWebService boCustomerWebService;

    @Override
    public AJAXCustomerView inactive(Long id) {
        ActionLogContext.put("INACTIVE_ID", id);
        return view(boCustomerWebService.inactive(id));
    }

    @Override
    public SearchCustomerAJAXResponse search(SearchCustomerAJAXRequest request) {
        BOSearchCustomerRequest req = new BOSearchCustomerRequest();
        req.email = request.email;
        req.firstName = request.firstName;
        req.lastName = request.lastName;
        req.limit = request.limit;
        req.skip = request.skip;
        BOSearchCustomerResponse search = boCustomerWebService.search(req);
        SearchCustomerAJAXResponse resp = new SearchCustomerAJAXResponse();
        resp.customers = new ArrayList<>();

        search.customers.forEach(cus -> {
            SearchCustomerAJAXResponse.Customer view = new SearchCustomerAJAXResponse.Customer();
            view.customerId = cus.customerId;
            view.email = cus.email;
            view.firstName = cus.firstName;
            view.lastName = cus.lastName;
            view.updatedTime = cus.updatedTime;
            resp.customers.add(view);
        });
        resp.total = search.total;
        return resp;
    }

    private AJAXCustomerView view(BOCustomerView boCustomerView) {
        AJAXCustomerView view = new AJAXCustomerView();
        view.customerId = boCustomerView.customerId;
        view.email = boCustomerView.email;
        view.firstName = boCustomerView.firstName;
        view.lastName = boCustomerView.lastName;
        view.updatedTime = boCustomerView.updatedTime;
        return view;
    }
}
