package app.web.service.order;

import app.bopratice.api.OrderAJAXWebService;
import app.bopratice.api.order.DeleteOrderAJAXResponse;
import app.bopratice.api.order.OrderStatusAJAXView;
import app.order.api.BOOrderWebService;
import app.order.api.order.BODeleteOrderResponse;
import core.framework.inject.Inject;

public class OrderAJAXWebServiceImpl implements OrderAJAXWebService {
    @Inject
    BOOrderWebService boOrderWebService;

    @Override
    public DeleteOrderAJAXResponse delete(Long id) {
        BODeleteOrderResponse delete = boOrderWebService.delete(id);
        DeleteOrderAJAXResponse response = new DeleteOrderAJAXResponse();
        response.orderId = delete.orderId;
        response.customerId = delete.customerId;
        response.productId = delete.productId;
        response.createTime = delete.createTime;
        response.finishTime = delete.finishTime;
        response.updatedTime = delete.updatedTime;
        response.orderStatus = OrderStatusAJAXView.valueOf(delete.orderStatus.name());
        return response;
    }

}
