package app.customer.api;

import app.customer.api.customer.BOCustomerView;
import app.customer.api.customer.BOSearchCustomerRequest;
import app.customer.api.customer.BOSearchCustomerResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface BOCustomerWebService {

    @GET
    @Path("/bo/customer")
    BOSearchCustomerResponse search(BOSearchCustomerRequest request);

    @PUT
    @Path("/bo/customer/:id/inactive-status")
    BOCustomerView inactive(@PathParam("id") Long id);
}
