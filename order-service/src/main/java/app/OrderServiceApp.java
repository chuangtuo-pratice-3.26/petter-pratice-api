package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

public class OrderServiceApp extends App {

    @Override
    protected void initialize() {
        http().httpPort(8889);
        load(new SystemModule("sys.properties"));
        load(new OrderModule());
    }
}
