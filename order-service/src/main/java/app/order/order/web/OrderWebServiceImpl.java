package app.order.order.web;

import app.order.api.OrderWebService;
import app.order.api.order.CreateOrderRequest;
import app.order.api.order.CreateOrderResponse;
import app.order.api.order.OrderView;
import app.order.order.service.OrderService;
import core.framework.inject.Inject;

public class OrderWebServiceImpl implements OrderWebService {
    @Inject
    OrderService orderService;

    @Override
    public CreateOrderResponse create(CreateOrderRequest request) {
        OrderView orderView = orderService.create(request);
        CreateOrderResponse response = new CreateOrderResponse();
        response.orderId = orderView.orderId;
        response.customerId = orderView.customerId;
        response.productId = orderView.productId;
        response.createTime = orderView.createTime;
        response.finishTime = orderView.finishTime;
        response.updatedTime = orderView.updatedTime;
        return response;
    }
}
