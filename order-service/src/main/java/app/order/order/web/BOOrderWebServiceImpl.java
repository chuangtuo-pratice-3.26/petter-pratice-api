package app.order.order.web;

import app.order.api.BOOrderWebService;
import app.order.api.order.BODeleteOrderResponse;
import app.order.api.order.BOOrderView;
import app.order.order.service.BOOrderService;
import core.framework.inject.Inject;

public class BOOrderWebServiceImpl implements BOOrderWebService {
    @Inject
    BOOrderService boOrderService;

    @Override
    public BODeleteOrderResponse delete(Long id) {
        BOOrderView delete = boOrderService.delete(id);
        BODeleteOrderResponse response = new BODeleteOrderResponse();
        response.orderId = delete.orderId;
        response.customerId = delete.customerId;
        response.productId = delete.productId;
        response.createTime = delete.createTime;
        response.finishTime = delete.finishTime;
        response.updatedTime = delete.updatedTime;
        return response;
    }
}
