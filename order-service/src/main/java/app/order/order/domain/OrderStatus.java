package app.order.order.domain;

import core.framework.db.DBEnumValue;

public enum OrderStatus {
    @DBEnumValue("ACTIVE")
    ACTIVE,
    @DBEnumValue("CLOSED")
    CLOSED,
    @DBEnumValue("FINISH")
    FINISH
}
