package app.order.order.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.LocalDateTime;

@Table(name = "orders")
public class Order {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "order_id")
    public Long orderId;

    @Column(name = "customer_id")
    public Long customerId;

    @Column(name = "product_id")
    public Long productId;

    @NotNull
    @Column(name = "order_status")
    public OrderStatus orderStatus;

    @NotNull
    @Column(name = "finish_time")
    public LocalDateTime finishTime;

    @NotNull
    @Column(name = "create_time")
    public LocalDateTime createTime;

    @NotNull
    @Column(name = "updated_time")
    public LocalDateTime updatedTime;
}
