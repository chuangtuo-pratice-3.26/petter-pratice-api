package app.order.order.service;

import app.order.api.order.CreateOrderRequest;
import app.order.api.order.OrderStatusView;
import app.order.api.order.OrderView;
import app.order.order.domain.Order;
import app.order.order.domain.OrderStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;

import java.time.LocalDateTime;

public class OrderService {
    @Inject
    Repository<Order> orderRepository;

    public OrderView create(CreateOrderRequest request) {
        Order order = new Order();
        order.customerId = request.customerId;
        order.productId = request.productId;
        order.orderStatus = OrderStatus.ACTIVE;
        order.createTime = LocalDateTime.now();
        order.updatedTime = LocalDateTime.now();
        order.finishTime = LocalDateTime.now();
        order.orderId = orderRepository.insert(order).orElseThrow();
        return view(order);
    }

    private OrderView view(Order order) {
        OrderView orderView = new OrderView();
        orderView.orderId = order.orderId;
        orderView.customerId = order.customerId;
        orderView.productId = order.productId;
        orderView.createTime = order.createTime;
        orderView.updatedTime = order.updatedTime;
        orderView.finishTime = order.finishTime;
        orderView.orderStatus = OrderStatusView.valueOf(order.orderStatus.name());
        return orderView;
    }
}
