package app.order.order.service;

import app.order.api.order.BOOrderView;
import app.order.api.order.OrderStatusView;
import app.order.order.domain.Order;
import app.order.order.domain.OrderStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

public class BOOrderService {
    @Inject
    Repository<Order> orderRepository;

    public BOOrderView delete(Long id) {
        Order order = orderRepository.get(id).orElseThrow(() -> new NotFoundException("target order not found order id is " + id));
        if (order.orderStatus.equals(OrderStatus.ACTIVE) || order.orderStatus.equals(OrderStatus.CLOSED)) {
            orderRepository.delete(id);
        }
        return view(order);
    }

    private BOOrderView view(Order order) {
        BOOrderView boOrderView = new BOOrderView();
        boOrderView.orderId = order.orderId;
        boOrderView.customerId = order.customerId;
        boOrderView.productId = order.productId;
        boOrderView.createTime = order.createTime;
        boOrderView.updatedTime = order.updatedTime;
        boOrderView.finishTime = order.finishTime;
        boOrderView.orderStatus = OrderStatusView.valueOf(order.orderStatus.name());
        return boOrderView;
    }
}
