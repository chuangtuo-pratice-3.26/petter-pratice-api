package app.order.api;

import app.order.api.order.CreateOrderRequest;
import app.order.api.order.CreateOrderResponse;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;

public interface OrderWebService {
    @POST
    @Path("/order")
    CreateOrderResponse create(CreateOrderRequest request);
}
