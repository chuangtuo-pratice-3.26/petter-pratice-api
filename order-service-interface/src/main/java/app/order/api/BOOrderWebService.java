package app.order.api;


import app.order.api.order.BODeleteOrderResponse;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface BOOrderWebService {
    @DELETE
    @Path("/bo/order/:id")
    BODeleteOrderResponse delete(@PathParam("id") Long id);
}
