package app.order.api.order;

import core.framework.api.json.Property;

public enum OrderStatusView {
    @Property(name = "ACTIVE")
    ACTIVE,
    @Property(name = "CLOSED")
    CLOSED,
    @Property(name = "FINISH")
    FINISH
}
