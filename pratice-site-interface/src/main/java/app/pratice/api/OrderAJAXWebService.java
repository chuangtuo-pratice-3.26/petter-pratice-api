package app.pratice.api;

import app.pratice.api.order.CreateOrderAJAXRequest;
import app.pratice.api.order.CreateOrderAJAXResponse;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;

public interface OrderAJAXWebService {
    @POST
    @Path("/ajax/order")
    CreateOrderAJAXResponse create(CreateOrderAJAXRequest request);
}
