package app.pratice.api.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;


public class CreateOrderAJAXRequest {
    @NotNull
    @Property(name = "customer_id")
    public Long customerId;

    @NotNull
    @Property(name = "product_id")
    public Long productId;
}
