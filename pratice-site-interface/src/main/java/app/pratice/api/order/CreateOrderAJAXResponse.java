package app.pratice.api.order;

import core.framework.api.json.Property;

import java.time.LocalDateTime;

public class CreateOrderAJAXResponse {
    @Property(name = "order_id")
    public Long orderId;

    @Property(name = "customer_id")
    public Long customerId;

    @Property(name = "product_id")
    public Long productId;

    @Property(name = "order_status")
    public OrderStatusAJAXView orderStatus;

    @Property(name = "finish_time")
    public LocalDateTime finishTime;

    @Property(name = "create_time")
    public LocalDateTime createTime;

    @Property(name = "updated_time")
    public LocalDateTime updatedTime;
}
