package app.pratice.api.order;

import core.framework.api.json.Property;

public enum OrderStatusAJAXView {
    @Property(name = "ACTIVE")
    ACTIVE,
    @Property(name = "CLOSED")
    CLOSED,
    @Property(name = "FINISH")
    FINISH
}
