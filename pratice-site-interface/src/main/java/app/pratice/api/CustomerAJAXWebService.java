package app.pratice.api;

import app.pratice.api.customer.CreateCustomerAJAXRequest;
import app.pratice.api.customer.CreateCustomerAJAXResponse;
import app.pratice.api.customer.GetCustomerAJAXResponse;
import app.pratice.api.customer.UpdateCustomerAJAXRequest;
import app.pratice.api.customer.UpdateCustomerAJAXResponse;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

public interface CustomerAJAXWebService {

    @GET
    @Path("/ajax/customer/:id")
    GetCustomerAJAXResponse get(@PathParam("id") Long id);

    @POST
    @Path("/ajax/customer")
    @ResponseStatus(HTTPStatus.CREATED)
    CreateCustomerAJAXResponse create(CreateCustomerAJAXRequest request);

    @PUT
    @Path("/ajax/customer/:id")
    UpdateCustomerAJAXResponse update(@PathParam("id") Long id, UpdateCustomerAJAXRequest request);
}
