import app.CustomerServiceApp;

public class Main {
    public static void main(String[] args) {
        new CustomerServiceApp().start();
    }
}
