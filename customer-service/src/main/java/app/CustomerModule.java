package app;

import app.customer.api.BOCustomerWebService;
import app.customer.api.CustomerWebService;
import app.customer.api.customer.BOCustomerView;
import app.customer.customer.domain.Customer;
import app.customer.customer.service.BOCustomerService;
import app.customer.customer.service.CustomerService;
import app.customer.customer.web.BOCustomerWebServiceImpl;
import app.customer.customer.web.CustomerWebServiceImpl;
import core.framework.module.Module;

import java.time.Duration;

public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        intit();
        binds();
        services();
    }

    private void intit() {
        db().repository(Customer.class);
        cache().remote(BOCustomerView.class, Duration.ofDays(1));
    }

    private void binds() {
        bind(CustomerService.class);
        bind(BOCustomerService.class);
    }

    private void services() {
        api().service(CustomerWebService.class, bind(CustomerWebServiceImpl.class));
        api().service(BOCustomerWebService.class, bind(BOCustomerWebServiceImpl.class));
    }
}
