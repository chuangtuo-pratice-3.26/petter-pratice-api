package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

public class CustomerServiceApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8888);
        load(new SystemModule("sys.properties"));
        load(new CustomerModule());
    }
}
