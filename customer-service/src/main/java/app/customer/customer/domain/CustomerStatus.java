package app.customer.customer.domain;

import core.framework.db.DBEnumValue;

public enum CustomerStatus {
    @DBEnumValue("ACTIVE")
    ACTIVE,
    @DBEnumValue("INACTIVE")
    INACTIVE
}
