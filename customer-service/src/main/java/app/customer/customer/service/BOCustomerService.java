package app.customer.customer.service;

import app.customer.api.customer.BOCustomerView;
import app.customer.api.customer.BOSearchCustomerRequest;
import app.customer.api.customer.BOSearchCustomerResponse;
import app.customer.customer.domain.Customer;
import app.customer.customer.domain.CustomerStatus;
import core.framework.cache.Cache;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.json.JSON;
import core.framework.log.Markers;
import core.framework.redis.Redis;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.stream.Collectors;

public class BOCustomerService {
    private final Logger logger = LoggerFactory.getLogger(BOCustomerService.class);
    @Inject
    Repository<Customer> customerRepository;
    @Inject
    Cache<BOCustomerView> cache;
    @Inject
    Redis redis;

    public BOSearchCustomerResponse search(BOSearchCustomerRequest request) {
        BOSearchCustomerResponse result = new BOSearchCustomerResponse();
        Query<Customer> query = customerRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.email)) {
            query.where("email = ?", request.email);
        }
        if (!Strings.isBlank(request.firstName)) {
            query.where("first_name like ?", Strings.format("{}%", request.firstName));
        }
        if (!Strings.isBlank(request.lastName)) {
            query.where("last_name like ?", Strings.format("{}%", request.lastName));
        }
        result.customers = query.fetch().stream().map(this::searchView).collect(Collectors.toList());
        result.total = (long) query.count();
        return result;
    }

    public BOCustomerView inactive(Long id) {
        try {
            Customer customer = customerRepository.get(id).orElseThrow(() -> new NotFoundException("target customer not found id is " + id));
            if (customer.status.equals(CustomerStatus.ACTIVE)) {
                customer.status = CustomerStatus.INACTIVE;
                customerRepository.update(customer);
            }
            BOCustomerView view = view(customer);
            redis.hash().set("inactive-customer-map", "customer-id:" + id, JSON.toJSON(view));
            redis.expire("inactive-customer-map", Duration.ofDays(1));
            return view;
        } catch (NotFoundException e) {
            logger.error(Markers.errorCode("CUSTOMER_NOT_FOUND"), "couldn't find the target customer");
            throw e;
        }
    }

    private BOSearchCustomerResponse.Customer searchView(Customer customer) {
        BOSearchCustomerResponse.Customer result = new BOSearchCustomerResponse.Customer();
        result.customerId = customer.customerId;
        result.email = customer.email;
        result.firstName = customer.firstName;
        result.lastName = customer.lastName;
        result.updatedTime = customer.updatedTime;
        return result;
    }

    private BOCustomerView view(Customer customer) {
        BOCustomerView result = new BOCustomerView();
        result.customerId = customer.customerId;
        result.email = customer.email;
        result.firstName = customer.firstName;
        result.lastName = customer.lastName;
        result.updatedTime = customer.updatedTime;
        return result;
    }

}
