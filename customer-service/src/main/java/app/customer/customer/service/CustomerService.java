package app.customer.customer.service;

import app.customer.api.customer.CreateCustomerRequest;
import app.customer.api.customer.CustomerView;
import app.customer.api.customer.UpdateCustomerRequest;
import app.customer.customer.domain.Customer;
import app.customer.customer.domain.CustomerStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;

public class CustomerService {
    @Inject
    Repository<Customer> customerRepository;

    public CustomerView create(CreateCustomerRequest request) {
        Customer customer = new Customer();
        customer.email = request.email;
        customer.firstName = request.firstName;
        customer.lastName = request.lastName;
        customer.status = CustomerStatus.ACTIVE;
        customer.updatedTime = LocalDateTime.now();
        customer.customerId = customerRepository.insert(customer).orElseThrow();
        return view(customer);
    }

    public CustomerView update(Long id, UpdateCustomerRequest request) {
        Customer customer = customerRepository.get(id).orElseThrow(() -> new NotFoundException("customer not found, id=" + id));
        customer.updatedTime = LocalDateTime.now();
        customer.firstName = request.firstName;
        if (request.lastName != null) {
            customer.lastName = request.lastName;
        }
        customerRepository.partialUpdate(customer);
        return view(customer);
    }

    public CustomerView get(Long id) {
        Customer customer = customerRepository.get(id).orElseThrow(() -> new NotFoundException("customer not found, id=" + id));
        return view(customer);
    }

    private CustomerView view(Customer customer) {
        CustomerView result = new CustomerView();
        result.customerId = customer.customerId;
        result.email = customer.email;
        result.firstName = customer.firstName;
        result.lastName = customer.lastName;
        result.updatedTime = customer.updatedTime;
        return result;
    }
}
