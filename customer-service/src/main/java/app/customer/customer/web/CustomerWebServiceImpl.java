package app.customer.customer.web;

import app.customer.api.CustomerWebService;
import app.customer.api.customer.CreateCustomerRequest;
import app.customer.api.customer.CreateCustomerResponse;
import app.customer.api.customer.CustomerView;
import app.customer.api.customer.GetCustomerResponse;
import app.customer.api.customer.UpdateCustomerRequest;
import app.customer.api.customer.UpdateCustomerResponse;
import app.customer.customer.service.CustomerService;
import core.framework.inject.Inject;

public class CustomerWebServiceImpl implements CustomerWebService {
    @Inject
    CustomerService customerService;

    @Override
    public CreateCustomerResponse create(CreateCustomerRequest request) {
        CustomerView customerView = customerService.create(request);
        var response = new CreateCustomerResponse();
        response.customerId = customerView.customerId;
        response.email = customerView.email;
        response.firstName = customerView.firstName;
        response.lastName = customerView.lastName;
        response.updatedTime = customerView.updatedTime;
        return response;
    }

    @Override
    public UpdateCustomerResponse update(Long id, UpdateCustomerRequest request) {
        CustomerView customerView = customerService.update(id, request);
        var response = new UpdateCustomerResponse();
        response.customerId = customerView.customerId;
        response.email = customerView.email;
        response.firstName = customerView.firstName;
        response.lastName = customerView.lastName;
        response.updatedTime = customerView.updatedTime;
        return response;
    }

    @Override
    public GetCustomerResponse get(Long id) {
        CustomerView customerView = customerService.get(id);
        var response = new GetCustomerResponse();
        response.customerId = customerView.customerId;
        response.email = customerView.email;
        response.firstName = customerView.firstName;
        response.lastName = customerView.lastName;
        response.updatedTime = customerView.updatedTime;
        return response;
    }
}
