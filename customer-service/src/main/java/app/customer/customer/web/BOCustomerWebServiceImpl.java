package app.customer.customer.web;

import app.customer.api.BOCustomerWebService;
import app.customer.api.customer.BOCustomerView;
import app.customer.api.customer.BOSearchCustomerRequest;
import app.customer.api.customer.BOSearchCustomerResponse;
import app.customer.customer.service.BOCustomerService;
import core.framework.inject.Inject;

public class BOCustomerWebServiceImpl implements BOCustomerWebService {
    @Inject
    BOCustomerService boCustomerService;

    @Override
    public BOSearchCustomerResponse search(BOSearchCustomerRequest request) {
        return boCustomerService.search(request);
    }

    @Override
    public BOCustomerView inactive(Long id) {
        return boCustomerService.inactive(id);
    }
}
