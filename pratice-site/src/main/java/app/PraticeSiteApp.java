package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

public class PraticeSiteApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8088);
        load(new SystemModule("app.properties"));
        load(new WebModule());
    }
}
