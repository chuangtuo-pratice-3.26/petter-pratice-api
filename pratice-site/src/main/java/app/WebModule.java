package app;

import app.customer.api.CustomerWebService;
import app.order.api.OrderWebService;
import app.pratice.api.CustomerAJAXWebService;
import app.pratice.api.OrderAJAXWebService;
import app.web.service.customer.CustomerAJAXWebServiceImpl;
import app.web.service.order.OrderAJAXWebServiceImpl;
import core.framework.module.Module;

public class WebModule extends Module {
    @Override
    protected void initialize() {
        dealClient();
        dealService();
    }

    private void dealClient() {
        api().client(CustomerWebService.class, requiredProperty("app.customerServiceURL"));
        api().client(OrderWebService.class, requiredProperty("app.orderServiceURL"));
    }

    private void dealService() {
        api().service(CustomerAJAXWebService.class, bind(CustomerAJAXWebServiceImpl.class));
        api().service(OrderAJAXWebService.class, bind(OrderAJAXWebServiceImpl.class));
    }
}
