package app.web.service.customer;

import app.customer.api.CustomerWebService;
import app.customer.api.customer.CreateCustomerRequest;
import app.customer.api.customer.CreateCustomerResponse;
import app.customer.api.customer.GetCustomerResponse;
import app.customer.api.customer.UpdateCustomerRequest;
import app.customer.api.customer.UpdateCustomerResponse;
import app.pratice.api.CustomerAJAXWebService;
import app.pratice.api.customer.CreateCustomerAJAXRequest;
import app.pratice.api.customer.CreateCustomerAJAXResponse;
import app.pratice.api.customer.GetCustomerAJAXResponse;
import app.pratice.api.customer.UpdateCustomerAJAXRequest;
import app.pratice.api.customer.UpdateCustomerAJAXResponse;
import core.framework.inject.Inject;

public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    CustomerWebService customerWebService;

    @Override
    public GetCustomerAJAXResponse get(Long id) {
        GetCustomerResponse get = customerWebService.get(id);
        GetCustomerAJAXResponse response = new GetCustomerAJAXResponse();
        response.customerId = get.customerId;
        response.email = get.email;
        response.firstName = get.firstName;
        response.lastName = get.lastName;
        response.updatedTime = get.updatedTime;
        return response;
    }

    @Override
    public CreateCustomerAJAXResponse create(CreateCustomerAJAXRequest request) {
        CreateCustomerRequest req = new CreateCustomerRequest();
        req.email = request.email;
        req.firstName = request.email;
        req.lastName = request.lastName;
        CreateCustomerResponse create = customerWebService.create(req);
        CreateCustomerAJAXResponse response = new CreateCustomerAJAXResponse();
        response.customerId = create.customerId;
        response.email = create.email;
        response.firstName = create.firstName;
        response.lastName = create.lastName;
        response.updatedTime = create.updatedTime;
        return response;
    }

    @Override
    public UpdateCustomerAJAXResponse update(Long id, UpdateCustomerAJAXRequest request) {
        UpdateCustomerRequest req = new UpdateCustomerRequest();
        req.firstName = request.firstName;
        req.lastName = request.lastName;
        UpdateCustomerResponse update = customerWebService.update(id, req);
        UpdateCustomerAJAXResponse response = new UpdateCustomerAJAXResponse();
        response.customerId = update.customerId;
        response.email = update.email;
        response.firstName = update.firstName;
        response.lastName = update.lastName;
        response.updatedTime = update.updatedTime;
        return response;
    }
}
