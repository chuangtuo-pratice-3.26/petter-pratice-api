package app.web.service.order;

import app.order.api.OrderWebService;
import app.order.api.order.CreateOrderRequest;
import app.order.api.order.CreateOrderResponse;
import app.pratice.api.OrderAJAXWebService;
import app.pratice.api.order.CreateOrderAJAXRequest;
import app.pratice.api.order.CreateOrderAJAXResponse;
import app.pratice.api.order.OrderStatusAJAXView;
import core.framework.inject.Inject;

public class OrderAJAXWebServiceImpl implements OrderAJAXWebService {
    @Inject
    OrderWebService orderWebService;

    @Override
    public CreateOrderAJAXResponse create(CreateOrderAJAXRequest request) {
        CreateOrderRequest req = new CreateOrderRequest();
        req.customerId = request.customerId;
        req.productId = request.productId;
        CreateOrderResponse create = orderWebService.create(req);
        CreateOrderAJAXResponse response = new CreateOrderAJAXResponse();
        response.orderId = create.orderId;
        response.productId = create.productId;
        response.customerId = create.customerId;
        response.finishTime = create.finishTime;
        response.createTime = create.createTime;
        response.updatedTime = create.updatedTime;
        response.orderStatus = OrderStatusAJAXView.valueOf(create.orderStatus.name());
        return response;
    }
}
